/*  News Post Script made by Lylac_SYS                    #
 #  This can load News Posts from an external source.     #
 #  See https://mspfa.com/?s=24896&p=2 for the tutorial.  */

(()=>{
  console.log(`loaded News Posts Galore`);
  window.NewsPosts={
    //Initial setup
    //STRING selector=target div | The parent div of where you want to put your posts.
    //NUMBER selectorChild=child div of target div | Where you actually want to place your posts.
    setupNews: (selector,selectorChild=0)=>{
      let temp=document.createElement('div');
      temp.id='adventureNews';
      document.querySelector(selector).insertBefore(temp, document.querySelector(selector).childNodes[selectorChild]);
    },
    /*
    News Post Crawler; Returns # of posts found in dir param
    STRING dir=web address | external source of your News Posts. ex: 'pipe.miroware.io/5bc1294f1c0a1c3ffd485c5b/Intervene/P/'
    STRING fileName=name of the file | example: -intervene
    The file MUST be an HTML file.
    NUMBER latestPostID=How many news post there are. | This is used to find your newest posts

    How to NOT use the fileName param properly: 71-intervene.html <-- Don't do this.
    The above will turn into this: https://www.google.com/7171-intervene.html.html
    */
    loadNews: ()=>{
      let np_vars,dir,fileName,latestPostID;
      try{
        np_vars=document.querySelector('#NewsPost_Vars').innerHTML.match(/\{(.*?)\}/g,);
        for(let i=0;i<3;i++){
          np_vars[i]=np_vars[i].slice(np_vars[i].indexOf('{')+1,np_vars[i].lastIndexOf('}'));
        }
        //np_vars=[,,]
        //np_vars=['bluh','bluh','bluh']
        //np_vars=['pipe.miroware.io/5bc1294f1c0a1c3ffd485c5b/Intervene/P/','-intervene','4']
        if (np_vars[0]===undefined||np_vars[1]===undefined||np_vars[2]===undefined) throw 'Your variables are Empty!';
        fetch(`https://${np_vars[0]+np_vars[2]+np_vars[1]}.html`,{cache:'no-store'})
        .then(r=>{if(r.status===404){throw 'URL returned a 404 response'}})
        .catch(e=>{return popError(`${e} : That website or news post doesn't Exist!`);});
        dir='https://'+np_vars[0];
        fileName=np_vars[1];
        latestPostID=np_vars[2];
      } catch(e) {popError(e)}

      console.log('[News Posts]: Loading latest news posts...');
      console.time('[News Posts]: Finished loading the latest posts in');
      console.log('[News Posts]: LatestPostID is currently: '+latestPostID);
      let i=0,np_load=()=>{
        if (i>=5){return console.timeEnd('[News Posts]: Finished loading the latest posts in');}i++;
        if (latestPostID-i+1<=0){return console.timeEnd('[News Posts]: Finished loading the latest posts in');}
        let np_url=`${dir}${latestPostID-i+1}${fileName}.html?cb=${Math.floor(Math.random()*Math.floor(99999999999))}`;
        // console.log(`[${i}]:${np_url}`);

        fetch(np_url,{cache:'no-store'})
        .then((r)=>{
          if (r.ok){
            return r.text().then(r=>{
              r=MSPFA.parseBBCode(r,false);
              r.innerHTML=r.innerHTML.replace(/&nbsp;/gi,' ')+`<br>`;
              r.className='newsPost';
              document.querySelector('#adventureNews').appendChild(r);
              np_load();
            });
          } else {
            console.warn('[News Posts]:WARNING: '+np_url+' was not added to #adventureNews');
          }
        })
      };
      np_load();
    }
  }

  let popError=(e)=>{
    let np_error = document.createElement('div');
    np_error.innerHTML=`${e}<br><br>Check your variables in your adventure info.<br><br>Consult <a href='https://mspfa.com/?s=24896'>NEWS POSTS GALORE</a> for more information.`;
    MSPFA.dialog('[News Posts]:ERROR:',np_error,['Okay']);
    return;
  }
})();
